import {
    BrowserRouter,
    Routes,
    Route,
  } from "react-router-dom";
  import MainPage from "../pages/mainPage";
import MainCart from "../pages/mainCart";
import Allorders from '../pages/AllOrders';
const Content = () => {
    return(
        <>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="/cart" element={<MainCart />} />
            <Route path="/all-orders" element={<Allorders />} />
          </Routes>
        </BrowserRouter>
      </>
    )
}

export default Content;