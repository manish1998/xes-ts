
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import '../utils/navbar.css';

const NavbarComponent = () => {
    return  <Navbar fixed="top" className="bg-body-tertiary">
                <Container>
                <img
                            src={"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTkkL8NebvnasyYPWuvXXpbWRXIuV4INu8SJA&usqp=CAU"}
                           width="70"
                            height="70"
                            className="d-inline-block align-top"
                            alt="Logo"
                        />
                        
                    
                    <Navbar.Brand href="#home">Manish Pizza House</Navbar.Brand>
                    <Navbar.Toggle />
                    <Navbar.Collapse className="justify-content-end">
                        <Navbar.Text>
                        Signed in as: <a href="#login"> User 1</a>
                        </Navbar.Text>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
}

export default NavbarComponent;