/*import React, { useState } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Pizza } from '../pages/pizza';
import '../utils/cart.css';


type ProductProps = {
  pizza: Pizza;
  addToCart: (pizza: Pizza,selectedSize:string) => void;
};

const Product: React.FC<ProductProps> = ({ pizza, addToCart }) => {
  const { name, description, priceRegularSize, priceMediumSize, priceLargeSize } = pizza;
  const [selectedSize, setSelectedSize] = useState('');

  const handleAddToCart = () => {
    
    addToCart(pizza,selectedSize);
  };

  const getPriceBySize = (size: string): number => {
    switch (size) {
      case 'Regular':
        return priceRegularSize;
      case 'Medium':
        return priceMediumSize;
      case 'Large':
        return priceLargeSize;
      default:
        return 0;
    }
  };

  return (
    <Card style={{ width: '15rem', margin: '10px', boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2)', borderRadius: '8px', border: '2px solid #f2f2f2' }}>
      <Card.Img variant="top" src={pizza.image_url} style={{ height: '200px', objectFit: 'cover', borderRadius: '8px 8px 0 0' }} />
      <Card.Body>
        <Card.Title style={{ fontWeight: 'bold', fontSize: '18px', marginBottom: '10px' }}>{name}</Card.Title>
        <Card.Text style={{ marginBottom: '10px' }}>{description}</Card.Text>
        <Card.Text>Type: {pizza.type}</Card.Text>
        <Card.Text style={{ marginTop: '20px' }}>
          <span style={{ fontSize: '16px', fontWeight: 'bold', marginRight: '10px' }}>Price:</span>
          <span style={{ fontSize: '16px', color: '#FF6B6B', fontWeight: 'bold' }}>Regular: ${priceRegularSize}</span>
          <span style={{ fontSize: '16px', color: '#FFC154', fontWeight: 'bold', marginLeft: '10px' }}>Medium: ${priceMediumSize}</span>
          <span style={{ fontSize: '16px', color: '#5DBB63', fontWeight: 'bold', marginLeft: '10px' }}>Large: ${priceLargeSize}</span>
        </Card.Text>
        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginTop: '20px' }}>
          <Button
            variant={selectedSize === 'Regular' ? 'danger' : 'outline-danger'}
            onClick={() => setSelectedSize('Regular')}
            style={{ borderRadius: '50%', width: '40px', height: '40px', padding: '0', fontSize: '16px' }}
          >
            R
          </Button>
          <Button
            variant={selectedSize === 'Medium' ? 'warning' : 'outline-warning'}
            onClick={() => setSelectedSize('Medium')}
            style={{ borderRadius: '50%', width: '40px', height: '40px', padding: '0', fontSize: '16px' }}
          >
            M
          </Button>
          <Button
            variant={selectedSize === 'Large' ? 'success' : 'outline-success'}
            onClick={() => setSelectedSize('Large')}
            style={{ borderRadius: '50%', width: '40px', height: '40px', padding: '0', fontSize: '16px' }}
          >
            L
          </Button>
        </div>
        {selectedSize && (
          <Button variant="primary" onClick={handleAddToCart} style={{ width: '100%', marginTop: '20px' }}>
            Add to Cart
          </Button>
        )}
      </Card.Body>
    </Card>
  );
};

export default Product; */
import React, { useState } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Pizza } from '../pages/pizza';

type ProductProps = {
  pizza: Pizza;
  addToCart: (pizza: Pizza, size: string) => void;
};

const Product: React.FC<ProductProps> = ({ pizza, addToCart }) => {
  const { name, description, priceRegularSize, priceMediumSize, priceLargeSize } = pizza;
  const [selectedSize, setSelectedSize] = useState('');

  const handleAddToCart = () => {
    addToCart(pizza, selectedSize);
  };

  return (
    <Card style={{ width: '15rem', margin: '10px', boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2)', borderRadius: '8px', border: '2px solid #f2f2f2' }}>
      <Card.Img variant="top" src={pizza.image_url} style={{ height: '200px', objectFit: 'cover', borderRadius: '8px 8px 0 0' }} />
      <Card.Body>
        <Card.Title style={{ fontWeight: 'bold', fontSize: '18px', marginBottom: '10px' }}>{name}</Card.Title>
        <Card.Text style={{ marginBottom: '10px' }}>{description}</Card.Text>
        <Card.Text>Type: {pizza.type}</Card.Text>
        <Card.Text style={{ marginTop: '20px' }}>
          <span style={{ fontSize: '16px', fontWeight: 'bold', marginRight: '10px' }}>Price:</span>
          <span style={{ fontSize: '16px', color: 'black', fontWeight: 'bold' }}>Regular: ${priceRegularSize}</span>
          <span style={{ fontSize: '16px', color: 'black', fontWeight: 'bold', marginLeft: '10px' }}>Medium: ${priceMediumSize}</span>
          <span style={{ fontSize: '16px', color: 'black', fontWeight: 'bold', marginLeft: '10px' }}>Large: ${priceLargeSize}</span>
        </Card.Text>
        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginTop: '20px' }}>
          <Button
            variant={selectedSize === 'Regular' ? 'danger' : 'outline-danger'}
            onClick={() => setSelectedSize('Regular')}
            style={{ borderRadius: '50%', width: '40px', height: '40px', padding: '0', fontSize: '16px' }}
          >
            R
          </Button>
          <Button
            variant={selectedSize === 'Medium' ? 'warning' : 'outline-warning'}
            onClick={() => setSelectedSize('Medium')}
            style={{ borderRadius: '50%', width: '40px', height: '40px', padding: '0', fontSize: '16px' }}
          >
            M
          </Button>
          <Button
            variant={selectedSize === 'Large' ? 'success' : 'outline-success'}
            onClick={() => setSelectedSize('Large')}
            style={{ borderRadius: '50%', width: '40px', height: '40px', padding: '0', fontSize: '16px' }}
          >
            L
          </Button>
        </div>
        {selectedSize && (
          <Button variant="primary" onClick={handleAddToCart} style={{ width: '100%', marginTop: '20px' }}>
      
            Add to Cart
          </Button>
        )}
      </Card.Body>
    </Card>
  );
};

export default Product;

