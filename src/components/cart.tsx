/*import { Card, ListGroup, Button } from 'react-bootstrap';
import { Pizza } from '../pages/pizza';
import { CartItem } from '../pages/pizza';




type CartProps = {
  cartItems: CartItem[];
};

const Cart: React.FC<CartProps> = ({ cartItems }) => {
  const calculateMinOrder = () => {
    return cartItems.length;
  };

  const calculateDeliveryCharge = () => {
    const deliveryChargePerPizza = 50;
    const numberOfPizzas = cartItems.length;
    return numberOfPizzas * deliveryChargePerPizza;
  };

  const hasCoupon = true;

  const calculateTotalOrderAmount = () => {
    const totalAmount = cartItems.reduce((sum, item) => sum + item.price, 0);
    const deliveryCharge = calculateDeliveryCharge();
    const couponDiscount = hasCoupon ? 25 : 0;

    return totalAmount + deliveryCharge - couponDiscount;
  };

  return (
    <Card className="cart-card">
      <Card.Body>
        <Card.Title>Your Order</Card.Title>
        {cartItems.length === 0 ? (
          <div className="empty-cart">
            <svg xmlns="http://www.w3.org/2000/svg" width="130" height="130" fill="currentColor" className="bi bi-cart" viewBox="0 0 16 16">
              <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
            </svg>
            <h5 className="empty-cart-text">Cart is empty. Add Products</h5>
          </div>
        ) : (
          <div>
            <ListGroup className="list-group-flush">
              <ListGroup.Item as="li" className="cart-item">Quantity: {calculateMinOrder()}</ListGroup.Item>
              <ListGroup.Item as="li" className="cart-item">Delivery Charge: {calculateDeliveryCharge()}</ListGroup.Item>
              <ListGroup.Item as="li" className="cart-item">I have coupon: {hasCoupon ? 'yes' : 'No'}</ListGroup.Item>
            </ListGroup>
            <p className="total-amount">Total Order Amount: ${calculateTotalOrderAmount()}</p>
            <Button className="create-order-btn" variant="primary">Create Order</Button>
          </div>
        )}
      </Card.Body>
    </Card>
  );
};

export default Cart; */
import React, { useState } from 'react';
import { Card, ListGroup, Button, Alert } from 'react-bootstrap';
import { Pizza } from '../pages/pizza';
import '../utils/cart.css';
import axios from 'axios';
import { Link } from 'react-router-dom';


 type CartItem = {
  pizza: Pizza;
  quantity: number;
  size: string;
  price: number;
};

type CartProps = {
  cartItems: CartItem[];
  removeItem: (pizzaId: number,size:string) => void;

  addToCart: (pizza: Pizza, size: string, quantity: number) => void;
  addToCartm: (pizza: Pizza, size: string, quantity: number) => void;
  
};

const Cart: React.FC<CartProps> = ({ cartItems, removeItem, addToCart, addToCartm}) => {
  const [updatedCartItems, setCartItems] = useState<CartItem[]>(cartItems);
  const [cart, setCart] = useState<CartItem[]>(cartItems);
 
  const  [showAlert, setShowAlert] = useState(false);
  const [orderData, setOrderData] = useState({
    customerId: 6,
    totalAmount: 0,
    deliveryAddress: '',
    pizza: cartItems.map((item) => ({
      pizzaId: item.pizza.pizzaId,
      quantity: item.quantity,
      size: item.size,
    })),
  });
  
  const createOrder = async () => {
    try {
      // Make the POST request to create the order
      console.log(cartItems);

      const order = {
        customerId: 6,

        totalAmount: calculateTotalOrderAmount(),

        deliveryAddress: 'techbulls ,kalyani nagar',
        pizza: cartItems.map((item) => ({
          pizzaId: item.pizza.pizzaId,
          quantity: item.quantity,
          size: item.size
        })),


      };

      console.log(order);

      setOrderData(order);
      const response = await axios.post('http://localhost:9999/orders', order);
      // const createdOrderData = response.data;
     
      setShowAlert(true);

      // Clear the cart by updating the cart items state to an empty array
      
      clearCart();
      // After showing the alert, clear the cart by calling the clearCart function
     
      // Handle the response or update the state as needed

    } catch (error) {
      console.error('Error creating order:', error);
    }
  };

  const handleCreateOrder = () => {
    if (cartItems.length > 0) {
      createOrder();
    }
  };


  const calculateTotalOrderAmount = (): number => {

    return cartItems.reduce((sum, item) => sum + item.price * item.quantity, 0);
  };


  const handleIncrement = (item: CartItem) => {
    addToCart(item.pizza, item.size, item.quantity + 1);
  };
  const clearCart = () => {
    // Clear the cart by updating the cartItems state to an empty array
    setCartItems([]);
  };

  return (
    <Card className="cart-card">
      <Card.Body>
        <Card.Title>Your Order</Card.Title>
        <Alert
          show={showAlert}
          variant="success"
          onClose={() => setShowAlert(false)}
          dismissible
        >
          Order successfully placed. Congrats!
        </Alert>

        {cartItems.length === 0 ? (
          <div className="empty-cart">
            <svg xmlns="http://www.w3.org/2000/svg" width="130" height="130" fill="currentColor" className="bi bi-cart" viewBox="0 0 16 16">
              <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
            </svg>
            <h5 className="empty-cart-text">Cart is empty. Add Products</h5>
          </div>
        ) : (
          <div>
            <ListGroup className="list-group-flush">
              {cartItems.map((item) => (
                <ListGroup.Item key={item.pizza.pizzaId} className="cart-item">
                  <div className="cart-item-details">
                    <div>
                      {/* <p>Quantity: {item.quantity}</p> */}
                      <p>Size: {item.size}</p>
                      <p>Pizza Name: {item.pizza.name}</p>
                    </div>
                    <div>
                      <p>Price: ${item.price}</p>
                    </div>
                    <div className="quantity-control">
                      <Button variant="" onClick={() => handleIncrement(item)}>
                        +
                      </Button>
                      <input type="number" value={item.quantity} min={1} style={{ width: "30px" }} />
                      <Button variant="" onClick={() => addToCartm(item.pizza, item.size, (item.quantity - 1))}>-</Button>
                    </div>
                  </div>
                  <Button className="remove-item-btn" variant="danger" onClick={() => removeItem(item.pizza.pizzaId, item.size)}>
                    Remove
                  </Button>
                </ListGroup.Item>
              ))}
            </ListGroup>
            <p className="total-amount">Total Order Amount: ${calculateTotalOrderAmount()}</p>

          </div>
        )}
        <Button style={{ marginTop: '20px' }} variant="primary" onClick={handleCreateOrder}>
          Create Order
        </Button>
        <Link to="/all-orders">
        <Button className="see-all-orders-btn" variant="secondary">
          See All Orders
        </Button>
      </Link>
      
      </Card.Body>
    </Card>
  );
};

export default Cart;






