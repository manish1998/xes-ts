import React, { useState, useEffect } from 'react';
import axios from 'axios';

interface Order {
  orderId: number;
  customerId: number;
  status: string;
  totalAmount: number;
  orderDateTime: string;
  deliveryAddress: string;
  pizza: {
    pizzaId: number;
    size: string;
    quantity: number;
    priceRegularSize: number; // Add this property for the pizza price with regular size
    priceMediumSize: number; // Add this property for the pizza price with medium size
    priceLargeSize: number; // Add this property for the pizza price with large size
  }[];
}

const Allorders = () => {
  const [orders, setOrders] = useState<Order[]>([]);
  const [editingOrder, setEditingOrder] = useState<Order | null>(null);
  const [editedSize, setEditedSize] = useState('');
  const [editedQuantity, setEditedQuantity] = useState(0);
  const [editedDeliveryAddress, setEditedDeliveryAddress] = useState('');
  const [totalAmount, setTotalAmount] = useState(0);

  useEffect(() => {
    fetchOrders();
  }, []);

  const fetchOrders = async () => {
    try {
      const response = await axios.get('http://localhost:9999/all'); // Replace with your API endpoint URL
      console.log('API Response:', response.data.data);
      if (Array.isArray(response.data.data)) {
        // Sort orders by orderId in descending order and get the first 10 elements
        const sortedOrders = response.data.data.sort(
          (a: { orderId: number }, b: { orderId: number }) => b.orderId - a.orderId
        ).slice(0, 10);
        setOrders(sortedOrders);
      } else {
        // Handle the case when the API response is not an array
        setOrders([]);
      }
    } catch (error) {
      console.error('Error fetching all orders:', error);
      // Handle the case when the API call fails
      setOrders([]);
    }
  };

  // Calculate total price based on editedSize and editedQuantity
  const calculateTotalPrice = () => {
    if (!editingOrder) {
      return 0;
    }
 
    let totalPrice = 0;
    for (const pizza of editingOrder.pizza) {
      let pizzaPrice = 0;
      switch (editedSize) {
        case 'Regular':
          pizzaPrice = pizza.priceRegularSize;
          break;
        case 'Medium':
          pizzaPrice = pizza.priceMediumSize;
          break;
        case 'Large':
          pizzaPrice = pizza.priceLargeSize;
          break;
        default:
          break;
      }
      totalPrice += pizzaPrice * editedQuantity;
    }

    return totalPrice;
  };

  useEffect(() => {
    // Calculate the total amount whenever editedSize or editedQuantity changes
    const totalPrice = calculateTotalPrice();
    setTotalAmount(totalPrice);
  }, [editingOrder, editedSize, editedQuantity]);

  const handleEditOrder = (order: Order) => {
    setEditingOrder(order);
    setEditedSize(order.pizza[0]?.size || '');
    setEditedQuantity(order.pizza[0]?.quantity || 0);
    setEditedDeliveryAddress(order.deliveryAddress);
  };

  const handleUpdateOrder = async () => {
    try {
      if (!editingOrder) {
        return;
      }

      
      const updatedOrder: Order = {
        ...editingOrder,
        deliveryAddress: editedDeliveryAddress, 
        pizza: editingOrder.pizza.map((pizza) => ({
          ...pizza,
          size: editedSize, // Update the size with the edited value
          quantity: editedQuantity, // Update the quantity with the edited value
        })),
      };

      // Send the updated order data to the API
      await axios.put(`http://localhost:9999/orders/${editingOrder.orderId}`, updatedOrder);

      // Update the local orders state with the updated order
      setOrders((prevOrders) =>
        prevOrders.map((order) => (order.orderId === editingOrder.orderId ? updatedOrder : order))
      );

      
      setEditingOrder(null);
    } catch (error) {
      console.error('Error updating order:', error);
      
    }
  };

  return (
    <div className="all-orders-container" style={allOrdersContainerStyle}>
      <h1>All Orders</h1>

      {orders.length === 0 ? (
        <p>No orders found.</p>
      ) : (
        orders.map((order) => (
          <div key={order.orderId} className="order-item" style={orderItemStyle}>
            <div className="order-container" style={orderContainerStyle}>
              <h3 style={orderIdStyle}>Order ID: {order.orderId}</h3>
              <p style={customerStyle}>Customer ID: {order.customerId}</p>
              <p style={statusStyle}>Status: {order.status}</p>
              <p style={totalAmountStyle}>
                Total Amount: ${editingOrder?.orderId === order.orderId ? totalAmount : order.totalAmount}
              </p>
              <p style={orderDateTimeStyle}>Order Date/Time: {order.orderDateTime}</p>
              {editingOrder?.orderId === order.orderId ? (
                <>
                  <input
                    type="text"
                    value={editedDeliveryAddress}
                    onChange={(e) => setEditedDeliveryAddress(e.target.value)}
                  />
                  <input
                    type="text"
                    value={editedSize}
                    onChange={(e) => setEditedSize(e.target.value)}
                  />
                  <input
                    type="number"
                    value={editedQuantity}
                    onChange={(e) => setEditedQuantity(Number(e.target.value))}
                  />
                  <button onClick={handleUpdateOrder}>OK</button>
                </>
              ) : (
                <>
                  <p style={deliveryAddressStyle}>Delivery Address: {order.deliveryAddress}</p>
                  <p>Size: {order.pizza[0]?.size}</p>
                  <p>Quantity: {order.pizza[0]?.quantity}</p>
                </>
              )}
              <div>
                <h4 style={pizzaHeaderStyle}>Pizzas:</h4>
                <ul style={pizzaListStyle}>
                  {order.pizza.map((pizza) => (
                    <li key={pizza.pizzaId} style={pizzaItemStyle}>
                      <span className="pizza-highlight" style={pizzaHighlightStyle}>
                        Size: {pizza.size}
                      </span>
                      , Quantity: {pizza.quantity}
                    </li>
                  ))}
                </ul>
              </div>
              {!editingOrder?.orderId && (
                <button onClick={() => handleEditOrder(order)}>Edit</button>
              )}
            </div>
          </div>
        ))
      )}
    </div>
  );
};

 

// Inline styles for each element
const allOrdersContainerStyle = {
backgroundColor: '#f0f0f0',
padding: '20px',
};

const headerStyle = {
textAlign: 'center',
marginBottom: '30px',
fontSize: '36px',
color: '#333',
};

const noOrdersStyle = {
textAlign: 'center',
fontSize: '24px',
color: '#666',
};

const orderItemStyle = {
display: 'inline-block',
width: '45%',
margin: '10px',
padding: '20px',
backgroundColor: '#fff',
borderRadius: '10px',
boxShadow: '0 3px 6px rgba(0, 0, 0, 0.16)',
};

const orderContainerStyle = {
padding: '10px',
};

const orderIdStyle = {
fontSize: '18px',
fontWeight: 'bold',
color: '#2e7d32',
};

const customerStyle = {
fontSize: '14px',
color: '#555',
};

const statusStyle = {
fontSize: '16px',
color: '#1976d2',
};

const totalAmountStyle = {
fontSize: '16px',
fontWeight: 'bold',
color: '#ff5722',
};

const orderDateTimeStyle = {
fontSize: '14px',
color: '#666',
};

const deliveryAddressStyle = {
fontSize: '14px',
color: '#888',
};

const pizzaHeaderStyle = {
fontSize: '16px',
color: '#222',
marginTop: '10px',
};

const pizzaListStyle = {
listStyle: 'none',
padding: '0',
marginTop: '5px',
};

const pizzaItemStyle = {
marginTop: '5px',
};

const pizzaHighlightStyle = {
backgroundColor: '#fbc02d',
padding: '2px 5px',
borderRadius: '4px',
color: '#333',
};

export default Allorders;