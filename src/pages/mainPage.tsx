/*import { Container, Row, Col, Card } from "react-bootstrap";
import Product from "../components/product";
import Cart from "../components/cart";
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Pizza } from '../pages/pizza';
import { CartItem } from '../pages/pizza';



const MainPage: React.FC = () => {
  const [pizzas, setPizzas] = useState<Pizza[]>([]);
  const [cartItems, setCartItems] = useState<CartItem[]>([]);

  useEffect(() => {
    fetchPizzas();
  }, []);

  const fetchPizzas = async () => {
    try {
      const response = await axios.get<Pizza[]>('http://localhost:9999/pizzas');
      console.log(response.data);
      setPizzas(response.data);
    } catch (error) {
      console.log('Error fetching pizzas:', error);
    }
  };

  const addToCart = (pizza: Pizza, selectedSize: string) => {
  const selectedPizza: CartItem = {
    pizza,
    price: getPriceBySize(pizza, selectedSize)
  };
  setCartItems([...cartItems, selectedPizza]);
};

const getPriceBySize = (pizza: Pizza, size: string) => {
  switch (size) {
    case 'Regular':
      return pizza.priceRegularSize;
    case 'Medium':
      return pizza.priceMediumSize;
    case 'Large':
      return pizza.priceLargeSize;
    default:
      return 0;
  }
};
  

  return (
    <Container style={{ marginTop: "100px", paddingTop: "60px", backgroundColor: "#EEE" }}>
      <Row>
        <Col xs sm={8} lg={8}>
          <h3>Pizza</h3>
        </Col>
        <Col xs sm={4} lg={4}>
          <div>
            <h4>Information</h4>
          </div>
        </Col>
      </Row>
      <Row>
        <Col xs sm={8} lg={8}>
          <Card style={{ backgroundColor: "#EEE" }}>
            <Row>
              {pizzas.map((pizza) => (
                <Col key={pizza.pizzaId}>
                  <Product pizza={pizza} addToCart={addToCart} />
                </Col>
              ))}
            </Row>
          </Card>
        </Col>
        <Col xs sm={4} lg={4}>
          <Card style={{ backgroundColor: "#EEE" }}>
            <Cart cartItems={cartItems} />
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default MainPage; */
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import { Pizza } from '../pages/pizza';
import Product from '../components/product';
import Cart from '../components/cart';


type CartItem = {
  pizza: Pizza;
  size: string;
  price: number;
  quantity: number;
};

const MainPage = () => {
  const [pizzas, setPizzas] = useState<Pizza[]>([]);
  const [cartItems, setCartItems] = useState<CartItem[]>([]);

  useEffect(() => {
    fetchPizzas();
  }, []);

  const fetchPizzas = async () => {
    try {
      const response = await axios.get('http://localhost:9999/pizzas');
      console.log(response.data);
      setPizzas(response.data);
    } catch (error) {
      console.log('Error fetching pizzas:', error);
    }
  };

  const addToCart = (pizza: Pizza, size: string) => {
    const existingCartItem = cartItems.find((item) => item.pizza.pizzaId === pizza.pizzaId && item.size === size);

    if (existingCartItem) {
      const updatedCartItems = cartItems.map((item) => {
        if (item.pizza.pizzaId === pizza.pizzaId && item.size === size) {
          return {
            ...item,
            quantity: item.quantity +1,
          };
        }
        return item;
      });

      setCartItems(updatedCartItems);
    } else {
      const newCartItem: CartItem = {
        pizza,
        size,
        price: getPriceBySize(pizza, size),
        quantity: 1,
      };

      setCartItems([...cartItems, newCartItem]);
    }
  };


  const addToCartm = (pizza: Pizza, size: string) => {
    const existingCartItem = cartItems.find((item) => item.pizza.pizzaId === pizza.pizzaId && item.size === size);

    if (existingCartItem) {
      const updatedCartItems = cartItems.map((item) => {
        if(item.quantity ===1)
        {
            removeItem(pizza.pizzaId, size)
        
        }
        else {
        if (item.pizza.pizzaId === pizza.pizzaId && item.size === size) {
          return {
            ...item,
            quantity: item.quantity -1,
          };
        }
    }
        return item;
      });

      setCartItems(updatedCartItems);
    } else {
      const newCartItem: CartItem = {
        pizza,
        size,
        price: getPriceBySize(pizza, size),
        quantity: 1,
      };

      setCartItems([...cartItems, newCartItem]);
    }
  };

  const getPriceBySize = (pizza: Pizza, size: string): number => {
    switch (size) {
      case 'Regular':
        return pizza.priceRegularSize;
      case 'Medium':
        return pizza.priceMediumSize;
      case 'Large':
        return pizza.priceLargeSize;
      default:
        return 0;
    }
  };

  const removeItem = (pizzaId: number, size: string) => {
    const updatedCartItems = cartItems.filter((item) => !(item.pizza.pizzaId === pizzaId && item.size === size));
    setCartItems(updatedCartItems);
  };
  
/*  const createOrder = async () => {
    try {
      
        
      const response = await axios.post('http://localhost:9999/orders', { items: cartItems },);
      console.log(response.data);
      alert('Order successfully placed!');
      setCartItems([]);
    } catch (error) {
      console.error(error);
      // Handle the error case, e.g., show error message to the user
    }
  };

  const handleCreateOrder = () => {
    createOrder();
  };*/

  return (
    <Container style={{ marginTop: '100px', paddingTop: '60px', backgroundColor: '#EEE' }}>
      <Row>
        <Col xs sm={8} lg={8}>
          <h3>Pizza</h3>
        </Col>
        <Col xs sm={4} lg={4}>
          <div>
            <h4>Information</h4>
          </div>
        </Col>
        <Col xs sm={8} lg={8}>
          <Card style={{ backgroundColor: '#EEE' }}>
            <Row>
              {pizzas.map((pizza) => (
                <Col key={pizza.pizzaId}>
                  <Product pizza={pizza} addToCart={addToCart} />
                </Col>
              ))}
            </Row>
          </Card>
        </Col>
        <Col xs sm={4} lg={4}>
          <Card style={{ backgroundColor: '#EEE' }}>
          <Cart cartItems={cartItems} removeItem={removeItem}  addToCart={addToCart}  addToCartm={addToCartm}  />
         
          </Card>
        </Col>
      </Row>
     
    </Container>
  );
};

export default MainPage;



