export interface Pizza  {
    pizzaId: number;
    name: string;
    description: string;
    priceRegularSize: number;
    priceMediumSize: number;
    priceLargeSize: number;
    type: string;
    image_url: string;
    price:number;
  };
/*  export interface CartItem {
    pizza: Pizza;
    price: number;
    size: string;
   quantity: number;
    

    
  };*/